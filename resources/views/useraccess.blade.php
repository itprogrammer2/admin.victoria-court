@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>User Access</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><strong>User Role</strong></div>
          <div class="card-body card-block">

                <table id="tbluserrole" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

          </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><strong>Resources List</strong></div>
          <div class="card-body card-block">

                <table id="tblresourceslist" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Resources</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

          </div>
        </div>
    </div>
  
</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    var tbluserrole;
    var tblresourceslist;

    $(document).ready(function(){

        LoadUserRoles();

    });

    function LoadUserRoles(){

        tbluserrole = $('#tbluserrole').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/useraccess/loaduserroles") }}',
            },
            columns : [
                {data: 'id', name: 'id'},
                {data: 'role', name: 'role'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

</script>
@endsection