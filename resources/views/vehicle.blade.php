@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Vehicles</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><button id="btnaddvehicle" name="btnaddvehicle" class="btn btn-success btn-flat" style="float: right;" data-toggle="modal" data-target="#newvehicle"><i class="fa fa-plus"></i> New Vehicle</button></div>
          <div class="card-body card-block">

            <table id="tblvehicle" class="table table-hover">
                <thead>
                    <tr>
                        <th>Vechile Name</th>
                        <th>Plate Number</th>
                        <th></th>
                    </tr>
                </thead>
            </table>

          </div>
        </div>
    </div>

    {{-- Modal --}}
    @include('modals.vehicle.newvehicle')
    @include('modals.vehicle.updatevehicle')
  
</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    //Variables
    var tblvehicle;

    $(document).ready(function(){

        LoadVehicleInformation();

    });

    $('#btnaddvehicle').on('click', function(){

        ClearNewVehicleInformation();

    });

    $('#btnnsave').on('click',async function(clickEvent){

       var name = $('#txtnname').val();
       var platenumber = $('#cmbnplatenumber').val();
       
       if(name==""){
            toastr.error('Please Input The Vehicle Name', '', { positionClass: 'toast-top-center' });
       }
       else if(platenumber==""){
            toastr.error('Please Select If The Plate Number Is Enable/Disable.', '', { positionClass: 'toast-top-center' });
       }
       else{
            
           $.ajax({
               url: '{{ url("api/vehicle/newvehicle") }}',
               type: 'post',
               data: {
                   name: name,
                   platenumber: platenumber
               },
               dataType: 'json',
               success: function(response){

                   if(response.success){

                       toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                       ReloadVehicleInformation();
                       $('#newvehicle .close').click();                      

                   }
                   else{
                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                   }

               }
           });

       }        
 
    });

    $(document).on('click', '#btnremove', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/vehicle/removevehicle") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadVehicleInformation();

                }

            }
        });

    });

    $(document).on('click', '#btnedit', function(){

        var id = $(this).val();
        $('#btnuupdate').val(id);
        
        $.ajax({
            url: '{{ url("api/vehicle/profilevehicle") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#txtuname').val(response.name);
                $('#cmbuplatenumber').val(response.platenumber);

            }
        });

    });

    $('#btnuupdate').on('click', function(){

        var id = $(this).val();
        var name = $('#txtuname').val();
        var platenumber = $('#cmbuplatenumber').val();

        if(name==""){
                toastr.error('Please Input The Vehicle Name', '', { positionClass: 'toast-top-center' });
        }
        else if(platenumber==""){
                toastr.error('Please Select If The Plate Number Is Enable/Disable.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/vehicle/updatevehicle") }}',
                type: 'post',
                data: {
                    id: id,
                    name: name,
                    platenumber: platenumber
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadVehicleInformation();
                        $('#updatevehicle .close').click();

                    }

                }
            });

        }
        

    });

    function LoadVehicleInformation(){

        tblvehicle = $('#tblvehicle').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/vehicle/loadvehicleinformation") }}',
            },
            columns : [
                {data: 'name', name: 'name'},
                {data: 'platenumber', name: 'platenumber'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ClearNewVehicleInformation(){

        $('#txtnname').val('');
        $('#cmbnplatenumber').val('');

    }

    function ReloadVehicleInformation(){

        tblvehicle.ajax.reload();

    }


</script>
@endsection