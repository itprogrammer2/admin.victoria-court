@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Reservation</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><button id="btnnewreservation" name="btnreservation" class="btn btn-flag btn-success" style="float: right;" data-toggle="modal" data-target="#newreservation"><i class="fa fa-plus"></i> New Reservation</button></div>
          <div class="card-body card-block">

                <table id="tblreservation" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

          </div>
        </div>
    </div>

    {{-- Modal --}}
    @include('modals.reservation.newreservation')
    @include('modals.reservation.updatereservation')
  
</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    //Variables
    var tblreservation;

    $(document).ready(function(){

        LoadReservationInformation();

    });

    $('#btnnewreservation').on('click', function(){

        ClearNewReservation();

    });

    $('#btnnsave').on('click',function(){

       var name = $('#txtnname').val();
       var description = $('#txtndescription').val();

       if(name==""){
            toastr.error('Please Input The Reservation Name', '', { positionClass: 'toast-top-center' });
       }
       else if(description==""){
            toastr.error('Please Input The Description', '', { positionClass: 'toast-top-center' });
       }
       else{

            $.ajax({
                url: '{{ url("api/reservation/newreservation") }}',
                type: 'post',
                data: {
                    name: name,
                    description: description
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadReservationInformation();
                        $('#newreservation .close').click();

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                        
                    }

                }
            });

       }

    });

    $(document).on('click', '#btnedit', function(){

        var id = $(this).val();
        $('#btnuupdate').val(id);

        $.ajax({
            url: '{{ url("api/reservation/getreservationprofile") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#txtuname').val(response.name);
                $('#txtudescription').val(response.description);

            }
        });
    
    });

    $(document).on('click', '#btnremove', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/reservation/removereservation") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function (response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadReservationInformation();

                }  

            }
        });


    });

    $('#btnuupdate').on('click', function(){

        var id = $(this).val();
        var name = $('#txtuname').val();
        var description = $('#txtudescription').val();

        UpdateReservation(id, name, description);

    });

    function UpdateReservation(id, name, description){

        if(name==""){
                toastr.error('Please Input The Reservation Name', '', { positionClass: 'toast-top-center' });
        }
        else if(description==""){
                toastr.error('Please Input The Description', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/reservation/updatereservation") }}' ,
                type: 'post',
                data: {
                    id: id,
                    name: name,
                    description: description
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadReservationInformation();
                        $('#updatereservation .close').click();

                    }

                }
            });
                    
        }

    }

    function LoadReservationInformation(){

        tblreservation = $('#tblreservation').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/reservation/loadreservationinformation") }}',
            },
            columns : [
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ClearNewReservation(){

        $('#txtnname').val('');
        $('#txtndescription').val('');

    }

    function ReloadReservationInformation(){

        tblreservation.ajax.reload();

    }

</script>
@endsection