@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Overview</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><strong>System Activities</strong></div>
          <div class="card-body card-block">

                <table id="tblsystemactivities" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Activity</th>
                            <th>Modified By</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                </table>

          </div>
        </div>
    </div>
  
</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    //Variables
    var tblsystemactivities;

    $(document).ready(function() {
     
        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
            toastr.success(msg);
        }

        LoadSystemActivities();

    });

    function LoadSystemActivities(){

        tblsystemactivities = $('#tblsystemactivities').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/systemactivities/loadsystemactivities") }}',
            },
            columns : [
                {data: 'activity', name: 'activity'},
                {data: 'modifiedby', name: 'modifiedby'},
                {data: 'date', name: 'date'},
                {data: 'time', name: 'time'},
            ]
        });

    }

</script>
@endsection