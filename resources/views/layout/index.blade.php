<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Victoria Court Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    
    <link rel="stylesheet" href="{{ asset("css/normalize.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bootstrap.css") }}">
    <link rel="stylesheet" href="{{ asset("css/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/themify-icons.css") }}">
    <link rel="stylesheet" href="{{ asset("css/flag-icon.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/cs-skin-elastic.css") }}">
    <link rel="stylesheet" href="{{ asset("css/toastr.min.css") }}">
    {{-- <link rel="stylesheet" href="{{ asset("css/bootstrap-select.less") }}"> --}}
    <link rel="stylesheet" href="{{ asset("css/lib/datatable/dataTables.bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("scss/style.css") }}">
    <link rel="stylesheet" href="{{ asset("css/waitMe.css") }}">
    <link rel="stylesheet" href="{{ asset("css/card.css") }}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset("css/select2.css") }}"> --}}
    <link rel="stylesheet" href="{{ asset("css/select2-bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/lightbox.css") }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


    <script src="{{ asset("js/jquery-3.3.1.js") }}"></script>
    <script src="{{ asset("js/popper.min.js") }}"></script>
    <script src="{{ asset("js/plugins.js") }}"></script>
    <script src="{{ asset("js/main.js") }}"></script>
    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("js/lib/data-table/datatables.min.js") }}"></script>
    <script src="{{ asset("js/lib/data-table/dataTables.bootstrap.min.js") }}"></script>
    <script src="{{ asset("js/select2.min.js") }}"></script>
    <script src="{{ asset("js/toastr.min.js") }}"></script>
    <script src="{{ asset("js/waitMe.js") }}"></script>
    <script src="{{ asset("js/lightbox.js") }}"></script>
    <script src="{{ asset("js/socket.io.js") }}"></script>



    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>


    <!-- Left Panel -->
    @include("includes.navbarleft")
    <!-- Left Panel -->

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        @include('includes.header')
        <!-- Header-->

        @section('content')
        @show
        
    </div><!-- /#right-panel -->
    <!-- Right Panel -->
    
    @section('scripts')
    @show

</body>
</html>
