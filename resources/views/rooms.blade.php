@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Rooms</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

   <div id="roomcontent" class="row">

   </div>


</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    $(document).ready(function(){

        LoadRooms();

    });

    

    function LoadRooms(){

        $.ajax({
            url: '{{ url("api/room/loadroomsinformation") }}',
            type: 'get',
            beforeSend: function(){
                $('#roomcontent').hide();
            },
            success: function(response){

                $('#roomcontent').html(response);

            },
            complete: function(){
                $('#roomcontent').fadeIn("slow");
            }
        });        

    }


</script>
@endsection