<div class="modal" id="agrouproomstatus" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
           <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Add Group Room Information</h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    
                    <div class="form-group">
                        <label for="cmbastatus">Status</label>
                        <select name="cmbastatus" id="cmbastatus" class="form-control" style="width: 300px;"></select>

                        
                    </div>
                
                    <button id="btnaaddstatus" name="btnaaddstatus" class="btn btn-primary" title="Add Status" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i></button>
                    <table id="tblavailablestatus" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 350px;">Available Status</th> 
                                <th style="width: 100px;"></th>
                            </tr>
                        </thead>
                        <tbody id="tablecontent">
                            <tr>
                                <td><select name="cmbaavailablestatus[]" id="cmbaavailablestatus1" class="form-control" style="width: 100%;"></select></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                
              </div>
             <div class="modal-footer">
                <button id="btnasave" name="btnasave" type="button" class="btn btn-primary">Save Information</button>
            </div>
        </div>
     </div>
</div>