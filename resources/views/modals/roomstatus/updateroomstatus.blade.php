<div class="modal" id="uroomstatus" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
           <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Room Status</h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
            </div>
               <div class="modal-body">

                <div class="form-group">

                    <label for="txturoomstatus">Status</label>
                    <input id="txturoomstatus" name="txturoomstatus" type="text" class="form-control" />
                    
                </div>

                <div class="form-group">
                    
                    <input id="txtucolor" name="txtucolor" type="color" class="form-control" style="width: 100px; height: 50px; float: right;"/>
                        
                </div>
                
              </div>
             <div class="modal-footer">
                <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
     </div>
</div>