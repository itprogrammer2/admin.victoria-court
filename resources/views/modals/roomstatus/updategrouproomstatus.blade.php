<div class="modal" id="ugrouproomstatus" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
               <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Update Group Room Information</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div id="updatecontent" class="container-fluid">
    
                        
                        <div class="form-group">

                            <label for="txtustatus">Status</label>
                            <input id="txtustatus" name="txtustatus" class="form-control" type="text" style="width: 300px;" readonly>
                            
                        </div>
                    
                        <button id="btnuaddstatus" name="btnuaddstatus" class="btn btn-primary" title="Add Status" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i></button>
                        <table id="tblavailablestatus" class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 350px;">Available Status</th> 
                                    <th style="width: 100px;"></th>
                                </tr>
                            </thead>
                            <tbody id="utablecontent">
                                
                            </tbody>
                        </table>
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnuupdate" name="btnuupdate" type="button" class="btn btn-primary">Update Information</button>
                    <button id="btnudelete" name="btnudelete" type="button" class="btn btn-danger">Delete</button>
                </div>
            </div>
         </div>
    </div>