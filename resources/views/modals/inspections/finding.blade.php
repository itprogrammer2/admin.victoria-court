<!-- Modal -->
<div class="modal fade" id="finding" tabindex="-1" role="dialog" aria-labelledby="roomInspection" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="roomInspection">Add Finding Type For Inspection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="col-md-6">
            <label>Finding Type</label><br/>
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control" id="inspectionFinding"/>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveFinding" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>