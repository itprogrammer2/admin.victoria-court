<div class="modal" id="useraccessroomstatusinfo" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
               <div id="useraccessmodaldocument" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Allow Status Information</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div class="container-fluid">
    
                        <div class="form-group col-md-6">
                            <label for="txturoomstatus">Role</label>
                            <input id="txturoomstatus" name="txturoomstatus" class="form-control" type="text" placeholder="Role" readonly>
                        </div>
    
                        <div class="col-md-12">
                            <table id="tblallowstatus" class="table table-striped table-bordered" style="width: 100%">
                                <thead> 
                                    <tr>
                                        <th>Allowed Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="allowstatusitems">

                                </tbody>
                            </table>
                        </div>

                    </div>
                    
                  </div>
                 <div class="modal-footer">
                </div>
            </div>
         </div>
    </div>