<div class="modal" id="updateinventory" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
           <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Update Inventory Information</h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    <div class="form-group">
                        <label for="txtuname">Name</label>
                        <input id="txtuname" name="txtuname" class="form-control" type="text" placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label for="txtudescription">Description</label>
                        <input id="txtudescription" name="txtudescription" class="form-control" type="text" placeholder="Description">
                    </div>

                </div>
                
              </div>
             <div class="modal-footer">
                <button id="btnuupdate" name="btnuupdate" type="button" class="btn btn-primary">Update Information</button>
            </div>
        </div>
     </div>
</div>