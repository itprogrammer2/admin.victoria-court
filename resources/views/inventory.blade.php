@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Inventory</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><button id="btnaddinventory" name="btnaddinventory" class="btn btn-flat btn-success" style="float: right;" data-toggle="modal" data-target="#newinventory"><i class="fa fa-plus"></i> Add Inventory</button></div>
          <div class="card-body card-block">

                <table id="tblinventory" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

          </div>
        </div>
    </div>

    {{-- Modal --}}
    @include('modals.inventory.newinventory')
    @include('modals.inventory.updateinventory')
  
</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    //Variables
    var tblinventory;

    $(document).ready(function(){

        LoadInventoryInformation();

    });

    $('#btnaddinventory').on('click', function(){

        ClearNewInventoryInformation();

    });

    $('#btnnsave').on('click', function(){

        var name = $('#txtnname').val();
        var description = $('#txtndescription').val();

        if(name==""){

            toastr.error('Please input the name of the item.', '', { positionClass: 'toast-top-center' });

        }
        else if(description==""){

            toastr.error('Please input the description of the item.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/inventory/newinventory") }}',
                type: 'post',
                data: {
                    name: name,
                    description: description
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadInventoryInformation();
                        $('#newinventory .close').click();    

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                        $('#txtnname').val('');
                        $('#txtnname').focus();

                    }

                }
            });

        }


    });

    $(document).on('click', '#btnremove', function(){

        var id = $(this).val();
        
        $.ajax({
            url: '{{ url("api/inventory/removeinventory") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadInventoryInformation();

                }

            }
        });

    });

    $(document).on('click', '#btnedit',function(){

       var id = $(this).val();
       $('#btnuupdate').val(id);
       
       $.ajax({
           url: '{{ url("api/inventory/profileinventory") }}',
           type: 'get',
           data: {
               id: id
           },
           dataType: 'json',
           success: function(response){

               $('#txtuname').val(response.name);
               $('#txtudescription').val(response.description);
               
           }
       });

    });

    $('#btnuupdate').on('click', function(){

      var id = $(this).val();
      var name = $('#txtuname').val();
      var description =  $('#txtudescription').val();

      if(name==""){

            toastr.error('Please input the name of the item.', '', { positionClass: 'toast-top-center' });

        }
        else if(description==""){

            toastr.error('Please input the description of the item.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/inventory/updateinventory") }}',
                type: 'post',
                data: {
                    id: id,
                    name: name,
                    description: description
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadInventoryInformation();
                        $('#updateinventory .close').click();

                    }

                }
            });

        }

    });

    function LoadInventoryInformation(){

        tblinventory = $('#tblinventory').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/inventory/loadinventoryinformation") }}',
            },
            columns : [
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ClearNewInventoryInformation(){

        $('#txtnname').val('');
        $('#txtndescription').val('');

    }

    function ReloadInventoryInformation(){

        tblinventory.ajax.reload();

    }

</script>
@endsection