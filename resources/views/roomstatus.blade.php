@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Room Status</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Room Status</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="roomstatus/grouproomstatus"><button style="float: right; margin-right: 10px;" class="btn btn-flat btn-info" title="Settings"><i class="fa fa-cog"></i></button></a>
                </div>
            <div class="card-body">

                <table id="tblroomstatus" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Room Status</th>
                            <th>Color</th>
                            {{-- <th></th> --}}
                        </tr>
                    </thead>
                </table>
    
            </div> {{-- Card Body --}}
            </div> {{-- Card --}}
        </div>


        </div>
    </div><!-- .animated -->


</div> <!-- .content -->


{{-- Modal --}}
@include('modals.roomstatus.updateroomstatus')


@endsection

@section('scripts')
<script>

    var tblroomstatus;
    $(document).ready(function() {
     
        RoomStatusInformation();

    });

    $('#btnupdate').on('click', function(){

        var id = $(this).val();
        var room_status = $('#txturoomstatus').val();
        var color = $('#txtucolor').val();

        if(room_status==""){
            toastr.error('Please input the room status.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/roomstatus/updateroomstatus") }}',
                type: 'post',
                data: {
                    id: id,
                    room_status: room_status,
                    color: color
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadRoomStatus();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                        $('#uroomstatus .close').click();
                        

                    }
                   

                }
            });

        }

    });

    $(document).on('click', '#btnedit', function(){
       
        var id = $(this).val();
        $('#btnupdate').val(id);
        
        $.ajax({
            url: '{{ url("api/roomstatus/roomstatusprofile") }}',
            type: 'get',
            data: {
                id: id,
            },
            dataType: 'json',
            success: function(response){
                
                $('#txturoomstatus').val(response.room_status);
                $('#txtucolor').val(response.color);

            },
        });

    });

    function RoomStatusInformation(){

        tblroomstatus = $('#tblroomstatus').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/roomstatus/roomstatusinformation") }}',
            },
            columns : [
                {data: 'roomstatus', name: 'roomstatus'},
                {data: 'color', name: 'color'},
                // {data: 'panel', name: 'panel'},
            ]
        });

    }

    function ReloadRoomStatus(){

        tblroomstatus.ajax.reload();

    }

</script>
@endsection