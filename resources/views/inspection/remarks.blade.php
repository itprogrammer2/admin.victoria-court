@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Remarks</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Room Status</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>

<div class="content mt-3">

    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="roomstatus/grouproomstatus"><button style="float: right; margin-right: 10px;" class="btn btn-flat btn-info" title="Settings"><i class="fa fa-cog"></i></button></a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#remarks">
					  Add Remarks
					</button>
                </div>
            <div class="card-body">

                <table id="tblinspectionremarks" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            {{-- <th></th> --}}
                        </tr>
                    </thead>
                </table>
    
            </div> {{-- Card Body --}}
            </div> {{-- Card --}}
        </div>


        </div>
	</div><!-- .animated -->


</div> <!-- .content -->

@include('modals.inspections.remarks')

@endsection

@section('scripts')
<script type="text/javascript">
	
	var tblinspectionremarks;

	$(document).ready(function(){
		reloadRemarks();
	});

	$('#saveRemarks').on('click', function(){
		var inspectionremarks = $('#inspectionremarks').val();
		$.ajax({
			url: '{{ url("api/inspection/remarks") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'inspectionremarks': inspectionremarks,
			},
			success: function(r){
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
				console.log(r);
			}
		}); 
	});

	function reloadRemarks(){
		tblinspectionremarks = $('#tblinspectionremarks').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/remarks/getInfo") }}'
			},
			columns: [
				{data: 'name', name: 'name'},
				// {data: 'description', name: 'description'},
			]
		});
	}

</script>
@endsection