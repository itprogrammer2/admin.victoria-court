@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Finding Type</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Room Status</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>

<div class="content mt-3">

    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="roomstatus/grouproomstatus"><button style="float: right; margin-right: 10px;" class="btn btn-flat btn-info" title="Settings"><i class="fa fa-cog"></i></button></a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#finding">
					  Add Finding Type
					</button>
                </div>
            <div class="card-body">

                <table id="tblinspectionfinding" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            {{-- <th></th> --}}
                        </tr>
                    </thead>
                </table>
    
            </div> {{-- Card Body --}}
            </div> {{-- Card --}}
        </div>


        </div>
	</div><!-- .animated -->


</div> <!-- .content -->

@include('modals.inspections.finding')

@endsection

@section('scripts')
<script type="text/javascript">
	
	var tblinspectionfinding;

	$(document).ready(function(){
		reloadFindings();
	});

	$('#saveFinding').on('click', function(){
		var inspectionFinding = $('#inspectionFinding').val();

		$.ajax({
			url: '{{ url("api/inspection/finding") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'inspectionFinding': inspectionFinding,
			},
			success: function(r){
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
			}
		});

	});

	function reloadFindings(){
		tblinspectionfinding = $('#tblinspectionfinding').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/finding/getInfo") }}'
			},
			columns: [
				{data: 'name', name: 'name'},
				// {data: 'description', name: 'description'},
			]
		});
	}

</script>
@endsection