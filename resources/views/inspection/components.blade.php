@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Components</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Eagle Eye</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>

<div class="content mt-3">
	
	<div class="animated fadeIn">
		<div class="card">
			<div class="card-header">
				<a href="#"><button style="float: right; margin-right: 10px;" class="btn btn-flat btn-info" title="Settings"><i class="fa fa-cog"></i></button></a>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#components">
				  Add Components
				</button>
			</div>
		</div>
		<div class="card-body">
			
			<table id="roomComponents" class="table table-striped table-bordered" style="width: 100%">
				<thead>
					<tr>
						<th>Component</th>
					</tr>
				</thead>
			</table>

		</div>
	</div>

</div>

{{-- Modal --}}
@include('modals.inspections.components')


@endsection

@section('scripts')
<script type="text/javascript">

	var roomcomponents;
	
	$(document).ready(function(){
		RoomComponents();
	});

	$('#saveComponents').on('click', function(){
		var componentLocation = $('#componentLocation').val();
		// console.log(componentLocation);
		$.ajax({
			url: '{{ url("api/inspection/component") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'component': componentLocation,
			},
			success:function(r){
				RoomComponents();
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
			}
		});
	});

	function RoomComponents(){
		roomcomponents = $('#roomComponents').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/component/getInfo") }}',
			},
			columns : [
				{data: 'name', name: 'name'},
			]
		});
	}

</script>
@endsection