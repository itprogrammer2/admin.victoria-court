@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Room</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Eagle Eye</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>

<div class="content mt-3">
	
	<div class="animated fadeIn">
		<div class="card">
			<div class="card-header">
				<a href="#"><button style="float: right; margin-right: 10px;" class="btn btn-flat btn-info" title="Settings"><i class="fa fa-cog"></i></button></a>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addRoomInspection">
				  Add Room
				</button>
			</div>
		</div>
		<div class="card-body">
			
			<table id="roomInspection" class="table table-striped table-bordered" style="width: 100%">
				<thead>
					<tr>
						<th>Room</th>
					</tr>
				</thead>
			</table>

		</div>
	</div>

</div>

{{-- Modal --}}
@include('modals.inspections.room')


@endsection

@section('scripts')
<script type="text/javascript">
	var roomtbl;

	$(document).ready(function(){
		reloadInpsectionRoom();
	});

	$('#saveLocation').on('click', function(){
		var inspectionlocation = $('#inspectionlocation').val();
		console.log(inspectionlocation);
		$.ajax({
			url: '{{ url('api/inspection/room') }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'room': inspectionlocation,
			},
			success:function(r){
				console.log(r);
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
			},
			error:function(r){
				console.log(r);
			}
		});
	});

	function reloadInpsectionRoom(){
		roomtbl = $('#roomInspection').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/room/getInfo") }}',
			},
			columns: [
				{data: 'name', name: 'name'},
			]
		});
	}

</script>
@endsection