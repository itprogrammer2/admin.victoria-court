@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Standard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Eagle Eye</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">
	
	<div class="animated fadeIn">
		<div class="card">
			<div class="card-header">
				<a href="#"><button style="float: right; margin-right: 10px;" class="btn btn-flat btn-info" title="Settings"><i class="fa fa-cog"></i></button></a>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#standard">
				  Add Standard
				</button>
			</div>
		</div>
		<div class="card-body">
			
			<table id="inpectionStandard" class="table table-striped table-bordered" style="width: 100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
					</tr>
				</thead>
			</table>

		</div>
	</div>

</div>

@include('modals.inspections.standards')

@endsection


@section('scripts')

<script type="text/javascript">
	
	var standardTable;

	$(document).ready(function(){
		reloadStandard();
	});

	$('#saveStandard').on('click', function(){
		var standardName = $('#standardName').val();
		var standardDescription = $('#standardDescription').val();
		// console.log(standardName + " " + standardDescription);
		$.ajax({
			url: '{{ url("/api/inspection/standard") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'standardName': standardName,
				'standardDescription': standardDescription,
			},
			success: function(r){
				reloadStandard();
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
				console.log(r);
			}	
		});
	});

	function reloadStandard(){
		standardTable = $('#inpectionStandard').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/standard/getInfo") }}'
			},
			columns: [
				{data: 'name', name: 'name'},
				{data: 'description', name: 'description'},
			]
		});
	}
</script>

@endsection