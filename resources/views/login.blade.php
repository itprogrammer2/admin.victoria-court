<!doctype html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Victoria Court Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">
    
        <link rel="stylesheet" href="{{ asset("css/normalize.css") }}">
        <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
        <link rel="stylesheet" href="{{ asset("css/font-awesome.min.css") }}">
        <link rel="stylesheet" href="{{ asset("css/themify-icons.css") }}">
        <link rel="stylesheet" href="{{ asset("css/flag-icon.min.css") }}">
        <link rel="stylesheet" href="{{ asset("css/cs-skin-elastic.css") }}">
        {{-- <link rel="stylesheet" href="{{ asset("css/bootstrap-select.less") }}"> --}}
        <link rel="stylesheet" href="{{ asset("css/toastr.min.css") }}">
        <link rel="stylesheet" href="{{ asset("scss/style.css") }}">
    
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    
        
        <script src="{{ asset("js/vendor/jquery-2.1.4.min.js") }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="{{ asset("js/plugins.js") }}"></script>
        <script src="{{ asset("js/main.js") }}"></script>    
        <script src="{{ asset("js/lib/chart-js/Chart.bundle.js") }}"></script>
        <script src="{{ asset("js/toastr.min.js") }}"></script>
    
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="#">
                        <img class="align-content" src="{{ asset("images/VCLogoLogin.png") }}" alt="" height="200px">
                    </a>
                </div>
                <div class="login-form">
                    <h2 style="text-align: center">ADMIN CMS</h2>
                    <form id="frmlogin" name="frmlogin" action="{{ url("api/login/loginuser") }}" method="POST" style="margin-top: 10px;">
                        <div class="form-group">
                            <label>Username</label>
                            <input id="txtusername" name="txtusername" type="text" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="txtpassword" name="txtpassword" type="password" class="form-control" placeholder="Password">
                        </div>
                    </form>
                    <button id="btnlogin" name="btnlogin" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function() {
     
            var msg = "{{ Session::get('message') }}";
            if(msg!=""){
                toastr.error(msg, '', { positionClass: 'toast-top-center' });
            }

            $('#txtusername').focus();

        });

        $('#btnlogin').on('click', function(){

            Login();

        });

        $('#txtpassword').on('keypress', function(e){

            if(e.keyCode==13){
                Login();
            }

        });

        function Login(){

            var username = $('#txtusername').val();
            var password = $('#txtpassword').val();

            if(username==""){
                toastr.error('Please input your username');
                $('#txtusername').focus();
            }
            else if(password==""){
                toastr.error('Please input your password');
                $('#txtpassword').focus();
            }
            else{

                $("#frmlogin").submit();

            }
  
        }
        
    </script>

</body>
</html>
