@foreach($roomimage as $image)
<div class="imagecard col-md-4" style="margin: 10px 10px 10px 10px;">
     <div class="fomr-group">
         <button id="btnimageremove" name="btnimageremove" class="btn" style="float: right; background-color:transparent;" value="{{ $image->id }}"><i class="fa fa-times"></i></button>
    </div>
    <a class="example-image-link" href="{{ Storage::url($image->image) }}" data-lightbox="{{ $image->image }}"><img src="{{ Storage::url($image->image) }}?{{ \Carbon\Carbon::now()->toDateTimeString() }}" class="rounded" alt="Avatar" style="width:100%; height: 140px;"></a>
    <div class="imagecontainer">
      <br>
    </div>
</div>
@endforeach

