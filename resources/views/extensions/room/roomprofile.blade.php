@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Room Details</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><a href="/rooms" style="float: right;"><i class="fa fa-backward"></i></a></div>
          <div class="card-body card-block">

                    <ul class="nav nav-tabs nav-justified">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#roomdetails"><strong>Room Details</strong></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#roominventory"><strong>Room Inventory</strong></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#roomimages"><strong>Room Images</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#statusoverride"><strong>Status Overide</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#roomrateordering"><strong>Room Rate Ordering</strong></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="roomdetails" class="container tab-pane active"><br>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="txtdroomname"><strong>Room Name</strong></label>
                                    <input id="txtdroomname" name="txtdroomname" class="form-control" type="text" placeholder="Room Name">
                                </div>
                                <div class="form-group">
                                    <label for="txtdroomtype"><strong>Room Type</strong></label>
                                    <input id="txtdroomtype" name="txtdroomtype" class="form-control" type="text" placeholder="Room Type" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="txtdroomnumber"><strong>Room Number</strong></label>
                                    <input id="txtdroomnumber" name="txtdroomnumber" class="form-control" type="text" placeholder="Room Number" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="txtdroomdescription"><strong>Description</strong></label>
                                    <textarea id="txtdroomdescription" name="txtdroomdescription" class="form-control" rows="4" placeholder="Room Description"></textarea>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <table class="table" style="width: 100%; margin-top: 30px;">
                                    <thead>
                                        <tr>
                                            <th style="width: 35%; font-size: 11px;">Routine</th>
                                            <th style="width: 35%; font-size: 11px;">Room Points</th>
                                            <th style="width: 40%; font-size: 11px;">Time (HH:MM)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="font-size: 10px; vertical-align: middle;"><strong>Regular Cleaning</strong></td>
                                            <td><input id="txtdregularpoints" name="txtdregularpoints" class="form-control" style="text-align: center; font-size: 12px;" type="text"></td>
                                            <td><div class="row"><input id="txtdregularhh" name="txtdregularhh" class="form-control" style="width: 50px; margin-right: 5px; text-align: center; font-size: 12px;" type="text" placeholder="HH"> <input id="txtdregularmm" name="txtdregularmm" class="form-control" style="width: 50px; text-align: center; font-size: 12px;" type="text" placeholder="MM"></div></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; vertical-align: middle;"><strong>Checkout Cleaning</strong></td>
                                            <td><input id="txtdcheckoutpoints" name="txtdcheckoutpoints" class="form-control" style="text-align: center; font-size: 12px;" type="text"></td>
                                            <td><div class="row"><input id="txtdcheckouthh" name="txtdcheckouthh" class="form-control" style="width: 50px; margin-right: 5px; text-align: center; font-size: 12px;" type="text" placeholder="HH"> <input id="txtdcheckoutmm" name="txtdcheckoutmm" class="form-control" style="width: 50px; text-align: center; font-size: 12px;" type="text" placeholder="MM"></div></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; vertical-align: middle;"><strong>General Cleaning</strong></td>
                                            <td><input id="txtdgeneralpoints" name="txtdgeneralpoints" class="form-control" style="text-align: center; font-size: 12px;" type="text"></td>
                                            <td><div class="row"><input id="txtdgeneralhh" name="txtdgeneralhh" class="form-control" style="width: 50px; margin-right: 5px; text-align: center; font-size: 12px;" type="text" placeholder="HH"> <input id="txtdgeneralmm" name="txtdgeneralmm" class="form-control" style="width: 50px; text-align: center; font-size: 12px;" type="text" placeholder="MM"></div></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="btndsaveroomdetails" name="btnsaveroomdetails" class="btn btn-info btn-flat" style="float: right;">Save Room Details</button>
                            </div>
                        </div>
                        <div id="roominventory" class="container tab-pane"><br>
                        
                            <div class="col-md-12">
                                <button id="btniadditem" name="btniadditem" style="float: right; margin-right: 13px; margin-bottom: 10px;" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button>
                            </div>
                     
                            <div id="newiitems" class="col-md-12">
                                <div class="col-md-6 form-group">
                                    <label for="cmbinvitem"><strong>Item</strong></label>
                                    <select name="cmbinvitem" id="cmbinvitem" class="form-control" style="width: 100%;">
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="txtiitemcode"><strong>Item Code</strong></label>
                                    <input id="txtiitemcode" name="txtiitemcode" type="text" class="form-control">
                                </div> 
                                
                                <div class="col-md-6 form-group">
                                    <label for="txtibrand"><strong>Brand</strong></label>
                                    <input id="txtibrand" name="txtibrand" type="text" class="form-control">
                                </div> 
                                <div class="col-md-6 form-group">
                                    <label for="txtibranddescription"><strong>Brand Description</strong></label>
                                    <input id="txtibranddescription" name="txtibranddescription" type="text" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                        <label for="txtiserialnumber"><strong>Serial Number</strong></label>
                                        <input id="txtiserialnumber" name="txtiserialnumber" type="text" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="txtitracknumber"><strong>Track Number</strong></label>
                                    <input id="txtitracknumber" name="txtitracknumber" type="text" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="txtidifferentiatevalue"><strong>Differentiate Value</strong></label>
                                    <input id="txtidifferentiatevalue" name="txtidifferentiatevalue" type="number" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="txtiactualcost"><strong>Actual Cost</strong></label>
                                    <input id="txtiactualcost" name="txtiactualcost" type="number" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                        <label for="txtinvqty"><strong>Quantity</strong></label>
                                        <input id="txtinvqty" name="txtinvqty" type="number" class="form-control">
                                </div>      
                                <div class="col-md-6">
                                    <label for="">&nbsp;</label>
                                    <button id="btninvadd" name="btninvadd" class="btn btn-block btn-info">Add</button>
                                </div>  
                            </div>
                                      
                            <div class="col-lg-12" id="tblroomi">
                                <table id="tblroominventory" class="table table-hover" style="width: 100%; margin-top: 10px; font-size: 11px;">
                                    <thead>
                                       <tr>
                                           <th>Item</th>
                                           <th>Item Description</th>
                                           <th>Item Code</th>
                                           <th>Brand</th>
                                           <th>Brand Description</th>
                                           <th>Serial #</th>
                                           <th>Track #</th>
                                           <th>Differentiate Value</th>
                                           <th>Actual Cost</th>
                                           <th>Qty</th>
                                           <th></th>
                                        </tr>
                                    </thead>
                               </table>
                            </div>
                            
                        </div>
                        <div id="roomimages" class="container tab-pane fade"><br>
                            
                            <div class="col-md-12">
                               <form id="frmuploadroomimage" name="frmuploadroomimage" enctype="multipart/form-data">
                                    <div class="form-group">
                                            <input id="txtuploadid" name="txtuploadid" type="text" value="{{ $id }}" hidden>
                                            <label for="uploadimage"><strong>Select Image</strong></label>
                                            <input id="uploadimage" name="uploadimage" type="file" class="form-control" alt="">
                                    </div>
                               </form>
                               <div class="form-group">
                                    <button id="btnuploadimage" name="
                                    btnuploadimage" type="button" class="btn btn-info btn-flat" style="float: right;">Upload Image</button>
                               </div>
                            </div>

                            <div id="imagecontent" class="col-md-12">

                                {{-- Image Content Here --}}

                            </div>
                        </div>
                        <div id="statusoverride" class="container tab-pane fade"><br>
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cmbostatus">Status</label>
                                    <select name="cmbostatus" id="cmbostatus" class="form-control" style="width: 100%;">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="txtoremarks">Overide Remarks</label>
                                    <textarea id="txtoremarks" name="txtoremarks" rows="4" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <button id="btnoupdateroom" name="btnoupdateroom" class="btn btn-info btn-flat" style="float: right;">Update Room</button>
                                </div>
                            </div>
                            <div class="col-md-3">

                            </div>
                        </div>
                        <div id="roomrateordering" class="container tab-pane fade"><br>

                            <div class="col-md-12">

                                    <table id="tblroomrateordering" class="table table-hover" style="font-size: 11px;">
                                            <thead>
                                                <tr>
                                                    <th>Order</th>
                                                    <th>Room Rate</th>
                                                    <th>Description</th>
                                                    <th>Count</th>
                                                    <th>Last Update</th>
                                                </tr>
                                            </thead>
                                    </table>
                                    
                            </div>
                                

                        </div>
                    </div>

          </div>
        </div>
    </div>
  
</div> <!-- .content -->
@endsection

@section('scripts')
<script>

    //Socket
    var ipaddress = "{{ $_SERVER['SERVER_ADDR'] }}";
    var socket = io.connect('http://'+ipaddress+':6969');

    //Variables
    var id = "{{ $id }}";
    var tblroominventory;
    var tblroomrateordering;
    var newitem = false;

    $(document).ready(function(){

        SetSelect2();
        SetDatatable();

        $('#newiitems').hide();
        $('#tblroomi').show();

        //Load
        LoadInventoryItems($('#cmbinvitem').attr('id'));
        LoadRoomInventory(id);
        LoadRoomImages(id);
        LoadStatusOveride($('#cmbostatus').attr('id'));

        $.ajax({
            url: '{{ url("api/room/loadroomprofile") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){
    
                $('#txtdroomname').val(response.data[0]["room_name"]);
                $('#txtdroomtype').val(response.data[0]["room_type"]);
                $('#txtdroomnumber').val(response.data[0]["room_no"]);
                $('#txtdroomdescription').val(response.data[0]["room_description"]);
                
            }
        });

    });

    $('#btndsaveroomdetails').on('click', function(){

        var roomname = $('#txtdroomname').val();
        var roomdescription = $('#txtdroomdescription').val();
        var roomno = $('#txtdroomnumber').val();

        $.ajax({
            url: '{{ url("api/room/updateroomdetails") }}',
            type: 'post',
            data: {
                id: id,
                roomname: roomname,
                roomdescription: roomdescription,
                roomno: roomno
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                }

            }
        });
    

    });

    $('#btninvadd').on('click', function(){

        var inventoryid = $('#cmbinvitem').val();
        var itemcode = $('#txtiitemcode').val();
        var brand = $('#txtibrand').val();
        var branddescription = $('#txtibranddescription').val();
        var serialnumber = $('#txtiserialnumber').val();
        var tracknumber = $('#txtitracknumber').val();
        var diffvalue = $('#txtidifferentiatevalue').val();
        var actualcost = $('#txtiactualcost').val();
        var qty = $('#txtinvqty').val();

        if(inventoryid==""){
            toastr.error('Please Select a Item.', '', { positionClass: 'toast-top-center' });
        }
        else if(qty==""){
            toastr.error('Please Input a Quantity.', '', { positionClass: 'toast-top-center' });
        }
        else if(qty<0){
            toastr.error('Please Dont Input a Negative Value.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/room/addroominventory") }}',
                type: 'post',
                data: {
                    id: id,
                    inventoryid: inventoryid,
                    itemcode: itemcode,
                    brand: brand,
                    branddescription: branddescription,
                    serialnumber: serialnumber,
                    tracknumber: tracknumber,
                    diffvalue: diffvalue,
                    actualcost: actualcost,
                    qty: qty
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadRoomInventory();

                        //Clear
                        $('#cmbinvitem').val('').trigger('change');
                        $('#txtiitemcode').val('');
                        $('#txtibrand').val('');
                        $('#txtibranddescription').val('');
                        $('#txtiserialnumber').val('');
                        $('#txtitracknumber').val('');
                        $('#txtidifferentiatevalue').val('');
                        $('#txtiactualcost').val('');
                        $('#txtinvqty').val('');

                        //Set
                        $('#newiitems').hide();
                        $('#tblroomi').show();
                        newitem = false;

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                        //Clear
                        $('#cmbinvitem').val('');


                    }

                }
            });

        }    

    });

    $(document).on('click', '#btninvremove', function(){
        
        var itemid = $(this).val();
        $.ajax({
            url: '{{ url("api/room/removeroominventory") }}',
            type: 'post',
            data: {
                id: itemid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadRoomInventory();

                }

            }
        });

    });

    $('#btnuploadimage').on('click', function(){

        var image = $('#uploadimage').val();

        if(image==""){
            toastr.error("Please select a image.", '', { positionClass: 'toast-top-center' });
        }
        else{

            var frm = document.getElementById("frmuploadroomimage");
            var fd = new FormData(frm);

            $.ajax({
                url: '{{ url("api/room/uploadroomimage") }}',
                type: 'post',
                data: fd,
                contentType: false,      
                cache: false,             
                processData: false, 
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        LoadRoomImages(id);

                        //Clear
                        $('#uploadimage').val('');

                    }

                }
            });

        }

    });

    $(document).on('click', '#btnimageremove', function(){

        var imageid = $(this).val();
        
        $.ajax({
            url: '{{ url("api/room/removeroomimage") }}',
            type: 'post',
            data: {
                imageid: imageid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    LoadRoomImages(id);

                }

            }
        });


    });

    $('#btnoupdateroom').on('click', function(){

        var overidestatusid = $('#cmbostatus').val();
        var overideremarks = $('#txtoremarks').val();

        if(overidestatusid==""){
            toastr.error("Please select a status.", '', { positionClass: 'toast-top-center' });
        }
        else if(overideremarks==""){
            toastr.error("Please input the overide remarks.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/room/overideroomstatus") }}',
                type: 'post',
                data: {
                    id: id,
                    overidestatusid: overidestatusid,
                    overideremarks: overideremarks
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                        //Clear
                        $('#cmbostatus').val('').trigger('change');
                        $('#txtoremarks').val('');

                        //Socket Emit
                        socket.emit('reloadtags', {
                            roomId: id,
                            btnid: 'roomid'+id
                        });

                    }

                }
            });

        }

    });

    $('#btniadditem').on('click', function(){

        if(newitem){
            $('#newiitems').hide();
            $('#tblroomi').show();
            newitem = false;
        }
        else{
            $('#newiitems').show();
            $('#tblroomi').hide();
            newitem = true;
        }
         
    });

    function LoadRooms(){

        $.ajax({
            url: '{{ url("api/room/loadroomsinformation") }}',
            type: 'get',
            beforeSend: function(){
                $('#roomcontent').hide();
            },
            success: function(response){

                $('#roomcontent').html(response);

            },
            complete: function(){
                $('#roomcontent').fadeIn("slow");
            }
        });        

    }

    function LoadInventoryItems(id){

        $.ajax({
            url: '{{ url("api/room/loadinvitems") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Please Select A Item</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#'+id).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["name"]+'</option>');
                }

            }
        });

    }

    function LoadRoomInventory(id){

        tblroominventory.destroy();
        tblroominventory = $('#tblroominventory').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/room/loadroominventory") }}',
                data: {
                    id: id
                },
            },
            columns : [
                {data: 'itemname', name: 'itemname'},
                {data: 'itemdescription', name: 'itemdescription'},
                {data: 'itemcode', name: 'itemcode'},
                {data: 'brand', name: 'brand'},
                {data: 'branddescription', name: 'branddescription'},
                {data: 'serialNumber', name: 'serialNumber'},
                {data: 'trackNumber', name: 'trackNumber'},
                {data: 'differentiateValue', name: 'differentiateValue'},
                {data: 'actualCost', name: 'actualCost'},
                {data: 'qty', name: 'qty'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function LoadRoomImages(id){

        $.ajax({
            url: '{{ url("api/room/loadroomimages") }}',
            type: 'get',
            data: {
                id: id
            },
            beforeSend: function(){
                $('#imagecontent').hide();
            },
            success: function(response){

                $('#imagecontent').html(response);

            },
            complete: function(){
                $('#imagecontent').fadeIn("slow");
            }
        });

    }
    
    function LoadStatusOveride(id){

        $.ajax({
            url: '{{ url("api/room/loadstatusoveride") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Please Select A Status</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#'+id).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["room_status"]+'</option>');
                }

            }
        });

    }

    function ReloadRoomInventory(){

        tblroominventory.ajax.reload();

    }

    function SetDatatable(){

        tblroominventory = $('#tblroominventory').DataTable();
        tblroomrateordering = $('#tblroomrateordering').DataTable();

    }

    function SetSelect2(){

        $('#cmbinvitem').select2({
            theme: 'bootstrap'
        });


        $('#cmbostatus').select2({
            theme: 'bootstrap'
        });
        
    }

</script>
@endsection