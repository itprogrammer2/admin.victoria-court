@extends('layout.index')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Settings</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        {{-- <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Room Status</li>
                </ol>
            </div>
        </div> --}}
    </div>
</div>


<div class="content mt-3">

    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="/roomstatus" style="float: right;"><i class="fa fa-backward"></i></a>
                    <button id="btnaddgroup" name="btnaddgroup" class="btn btn-success" data-toggle="modal" data-target="#agrouproomstatus"><i class="fa fa-plus"> Add Group</i></button>
                </div>
            <div class="card-body">
                          
                <table id="tblgrouproomstatus" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Room Status</th>
                            <th>Available Status</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
    
            </div> {{-- Card Body --}}
            </div> {{-- Card --}}
        </div>


        </div>
    </div><!-- .animated -->


</div> <!-- .content -->


{{-- Modal --}}
@include('modals.roomstatus.addgrouproomstatus')
@include('modals.roomstatus.updategrouproomstatus')

@endsection

@section('scripts')
<script>

    var tblgrouproomstatus;

    //Add Group Status
    var additem = 1;
    var additemarray = new Array();
    var sroom_status_id;

    //Update Group Status
    var addupdateitem = 1;
    var addupdateitemarray = new Array();
    var updatedeletearray = new Array();
    var uroom_status_id;

    $(document).ready(function() {
     
        SetSelect2();
        GroupRoomStatusInformation();

    });

    function GroupRoomStatusInformation(){

        tblgrouproomstatus = $('#tblgrouproomstatus').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/roomstatus/grouproomstatusinformation") }}',
            },
            columns : [
                {data: 'roomstatus', name: 'roomstatus'},
                {data: 'availablestatus', name: 'availablestatus'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    $('#btnaddgroup').on('click', function(){

        $('#cmbaavailablestatus1').find('option').remove();
        ClearAddGroupStatus();
        LoadRoomStatus($('#cmbastatus').attr('id'));
       

    });

    $('#cmbastatus').on('change', function(){

        sroom_status_id = $(this).val();
        LoadAvailableRoomStatus($('#cmbaavailablestatus1').attr('id'), sroom_status_id);
        ClearAddGroupStatus();

    });

    $('#btnaaddstatus').on('click', function(){

        additem += 1;
        additemarray.push(additem);
        $('#tablecontent').append('<tr id='+additem+'><td><select name="cmbaavailablestatus[]" id="cmbaavailablestatus'+additem+'" class="form-control" style="width: 100%;"></select></td><td><button id="btnaremove" class="btn btn-flat btn-danger" style="float: right;" value="'+additem+'"><i class="fa fa-trash"></i></button></td></tr>');
        LoadAvailableRoomStatus($('#cmbaavailablestatus'+additem).attr('id'), sroom_status_id);
        

    });

    $(document).on('click', '#btnaremove', function(){

        var id = $(this).val();
        $('#'+id).remove();

    });

    $(document).on('change', '[name="cmbaavailablestatus[]"]', function(){

        var val = $(this).val();
        var data = $("[name='cmbaavailablestatus[]']").serializeArray();
        var count = 0;
        for(var i=0;i<data.length;i++){ //Validation
           if(data[i].value==val){
              count ++;
           }
        }

        if(count!=1){

            $(this).val('');
            toastr.error("This item has already been selected.", '', { positionClass: 'toast-top-center' });

        }

    });

    $('#btnasave').on('click', function(clickEvent){

        var available_status_blank = false;
        var room_status_id = $('#cmbastatus').val();
        var available_status_id = $('[name="cmbaavailablestatus[]"]').serializeArray();

        for(var i=0;i<available_status_id.length;i++){
            
            if(available_status_id[i].value==""){
                available_status_blank = true;
                break;
            }

        }

        if(available_status_blank){

            toastr.error("Please select a available room status.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/roomstatus/savegrouproomstatus") }}',
                type: 'post',
                data: {
                    room_status_id: room_status_id,
                    available_status_id: available_status_id
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadGroupRoomStatus();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#agrouproomstatus .close').click();

                    }
                    else{
                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                    }
                    
                }
            });

        }

    });

    $(document).on('click',  '#btnedit',  function(){

        uroom_status_id = $(this).val();
        updatedeletearray = [];
        ClearUpdateGroupStatus();
        $('#btnudelete').val(uroom_status_id);

        $.ajax({
            url: '{{ url("api/roomstatus/loadroomstatusprofile") }}',
            type: 'get',
            data: {
                id: uroom_status_id,
            },
            dataType: 'json',
            beforeSend: function(){

                $('#updatecontent').waitMe({

                    effect : 'roundBounce',
                    text : '',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000'

                });

            },
            success: function(response){

                
                $('#txtustatus').val(response.roomstatus[0]["room_status"]);
                for(var i=0;i<response.roomprofile.length;i++){

                    if(i==0){
                        $('#utablecontent').append('<tr id="'+response.roomprofile[i]["id"]+'"><td><input type="hidden" id="txtuid" name="txtuid[]" value="'+response.roomprofile[i]["id"]+'"><select name="cmbuavailablestatus[]" id="cmbuavailablestatus'+response.roomprofile[i]["id"]+'" class="form-control" style="width: 100%;"></select></td><td></td></tr>');
                    }
                    else{
                        $('#utablecontent').append('<tr id="'+response.roomprofile[i]["id"]+'"><td><input type="hidden" id="txtuid" name="txtuid[]" value="'+response.roomprofile[i]["id"]+'"><select name="cmbuavailablestatus[]" id="cmbuavailablestatus'+response.roomprofile[i]["id"]+'" class="form-control" style="width: 100%;"></select></td><td><button id="btnuremoveedit" class="btn btn-flat btn-danger" style="float: right;" value="'+response.roomprofile[i]["id"]+'"><i class="fa fa-trash"></i></button></td></tr>');
                    }

                    LoadUAvailableRoomStatus($('#cmbuavailablestatus'+response.roomprofile[i]["id"]).attr('id'), uroom_status_id, response.roomprofile[i]["available_status_id"], i, response.roomprofile.length);

                }

            }
        });

    });

    $(document).on('click', '#btnuremoveedit', function(){

        var id = $(this).val();
        updatedeletearray.push(id);
        $('#'+id).remove();

    });

    $('#btnuupdate').on('click', function(clickEvent){

        var groupid = $('[name="txtuid[]"]').serializeArray();
        var available_status_id = $('[name="cmbuavailablestatus[]"]').serializeArray();

        $.ajax({
            url: '{{ url("api/roomstatus/updateroomgroupstatus") }}',
            type: 'post',
            data: {
                id: uroom_status_id,
                groupid: groupid,
                available_status_id: available_status_id,
                updatedeletearray: updatedeletearray
            },
            dataType: 'json',
            success: function(response){

                ReloadGroupRoomStatus();
                toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                $('#ugrouproomstatus .close').click();
               

            }
        });

    });

    $('#btnudelete').on('click', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/roomstatus/deleteroomgroupstatus") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadGroupRoomStatus();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    $('#ugrouproomstatus .close').click();

                }

            }
        });

    });

    $('#btnuaddstatus').on('click', function(){

        $('#utablecontent').append('<tr id="'+addupdateitem+'"><td><input type="hidden" id="txtuid" name="txtuid[]" value="0"><select name="cmbuavailablestatus[]" id="cmbuavailablestatusadd'+addupdateitem+'" class="form-control" style="width: 100%;"></select></td><td><button id="btnuremoveadd" class="btn btn-flat btn-danger" style="float: right;" value="'+addupdateitem+'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadAvailableRoomStatus($('#cmbuavailablestatusadd'+addupdateitem).attr('id'), uroom_status_id);
        addupdateitem += 1;
        addupdateitemarray.push(addupdateitem);

    });

    $(document).on('click', '#btnuremoveadd', function(){

        var id = $(this).val();
        $('#'+id).remove();

    });

    $(document).on('change', '[name="cmbuavailablestatus[]"]', function(){

            var val = $(this).val();
            var data = $("[name='cmbuavailablestatus[]']").serializeArray();
            var count = 0;
            for(var i=0;i<data.length;i++){ //Validation
            if(data[i].value==val){
                count ++;
            }
            }

            if(count!=1){

                $(this).val('');
                toastr.error("This item has already been selected.", '', { positionClass: 'toast-top-center' });

            }

        });

    function LoadRoomStatus(id){

        $.ajax({
            url: '{{ url("api/roomstatus/loadroomstatus") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select A Room Status</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["id"] +'">'+  response.data[i]["room_status"] +'</option>');
                }

            }
        });

    }

    function LoadUAvailableRoomStatus(id, room_status_id, value, itemload, itemcount){
     
        $.ajax({
            url: '{{ url("api/roomstatus/loadroomstatus") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select A Room Status</option>');
                for (var i = 0; i < response.data.length; i++) {

                    if(room_status_id!=response.data[i]["id"]){
                        $('#'+id).append('<option value="'+  response.data[i]["id"] +'">'+  response.data[i]["room_status"] +'</option>');
                    }
                    
                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){
                $('#'+id).val(value).trigger('change');
                if(itemload+1==itemcount){
                      $('#updatecontent').waitMe('hide');
                }
            }
        });

    }

    function LoadAvailableRoomStatus(id, room_status_id){

        $.ajax({
            url: '{{ url("api/roomstatus/loadroomstatus") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select A Room Status</option>');
                for (var i = 0; i < response.data.length; i++) {

                    if(room_status_id!=response.data[i]["id"]){
                        $('#'+id).append('<option value="'+  response.data[i]["id"] +'">'+  response.data[i]["room_status"] +'</option>');
                    }
                    
                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function ClearAddGroupStatus(){

        for(var i=0;i<additemarray.length;i++){
            $('#'+additemarray[i]).remove();
        }
        additem = 1;
        additemarray = [];

    }

    function ClearUpdateGroupStatus(){

         $('#utablecontent').find('tr').remove();

    }

    function ReloadGroupRoomStatus(){

        tblgrouproomstatus.ajax.reload();

    }

    function SetSelect2(){

        $('#cmbastatus').select2({
            theme: 'bootstrap'
        });

        $('#cmbaavailablestatus1').select2({
            theme: 'bootstrap'
        });
        
    }


</script>
@endsection