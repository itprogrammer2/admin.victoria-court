<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="{{ asset("images/VCLogo.png") }}" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./">VC</a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <h3 class="menu-title">Navigation</h3><!-- /.menu-title -->
                <li>
                    <a href="/main"> <i class="menu-icon fa fa-tasks"></i>Overview </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-money"></i>Expenses</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-angle-double-right"></i><a href="ui-buttons.html">Upload Expenses</a></li>
                        <li><i class="fa fa-angle-double-right"></i><a href="ui-badges.html">Expenses Settings</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-money"></i>Budgets</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-angle-double-right"></i><a href="tables-basic.html">Upload Budgets</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/useraccess"> <i class="menu-icon fa fa-user"></i>User Access </a>
                </li>
                <li>
                    <a href="/rooms"> <i class="menu-icon fa fa-tasks"></i>Rooms </a>
                </li>
                <li>
                    <a href="/roomstatus"> <i class="menu-icon fa fa-tasks"></i>Rooms Status </a>
                </li>
                <li>
                    <a href="/vehicle"> <i class="menu-icon fa fa-truck"></i>Vehicles </a>
                </li>
                <li>
                    <a href="/reservation"> <i class="menu-icon fa fa-book"></i>Reservation </a>
                </li>
                <li>
                    <a href="/inventory"> <i class="menu-icon fa fa-dropbox"></i>Inventory </a>
                </li>
                <li>
                    <a href="#"> <i class="menu-icon fa fa-tasks"></i>Kiosk </a>
                </li>
                <li>
                    <a href="#"> <i class="menu-icon fa fa-cog"></i>Settings </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-money"></i>Eagle Eye</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-angle-double-right"></i><a href="/eagleye">Room Inspection</a></li>
                        <li><i class="fa fa-angle-double-right"></i><a href="/components">Components</a></li>
                        <li><i class="fa fa-angle-double-right"></i><a href="/standard">Standard</a></li>
                        <li><i class="fa fa-angle-double-right"></i><a href="/remarks">Remarks</a></li>
                        <li><i class="fa fa-angle-double-right"></i><a href="/findings">Findings</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->