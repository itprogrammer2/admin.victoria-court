<header id="header" class="header">

    <div class="header-menu">
        
        <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
        </div>

        <div class="col-sm-5">
            {{-- <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-sign-out"></i>
                </a>

                <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                        <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                        <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                        <a class="nav-link" href="#"><i class="fa fa-power -off"></i>Logout</a>
                </div>
            </div> --}}
            
            <div class="user-area dropdown float-right" style="margin-top: 7px;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   Hello: {{ Auth::user()->username }}
                </a>

                <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="#" id="btnlogout"><i class="fa fa-sign-out"></i>Logout</a>
                </div>
            </div>   

        </div>
    </div>

</header><!-- /header -->
<script>

    $('#btnlogout').on('click', function(){

        $.ajax({
           url: '{{ url("api/login/logoutuser") }}',
           type: 'post',
           success: function(){

               window.location = "/login";

           } 
        });

    });

</script>