<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RoomStatus_Model extends Model
{
    
    protected $table = "roomstatus";
    protected $connection = "rmsnew";

    public static function GroupRoomStatusInformation(){

        $result = DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->select('a.id', 'a.room_status', DB::raw("GROUP_CONCAT(b.room_status SEPARATOR ', ') AS 'available_room_status'"))
        ->leftjoin('roomstatus as a', 'tblroomgroupstatus.room_status_id', '=', 'a.id')
        ->leftjoin('roomstatus as b', 'tblroomgroupstatus.available_status_id', '=', 'b.id')
        ->groupBy('a.id','a.room_status')
        ->get();

        return $result;

    }

    public static function SaveGroupRoomStatus($room_status, $available_room_status){

        $result = DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->insert([
            "room_status_id"=>$room_status,
            "available_status_id"=>$available_room_status,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateGroupRoomStatus($room_status){

        $result = DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->select(DB::raw("COUNT(*) AS 'status_count'"))
        ->where('room_status_id', '=', $room_status)
        ->get();

        if($result[0]->status_count==0){
            return false;
        }
        else{
            return true;
        }

    }

    public static function LoadRoomStatusProfile($room_status){

        $result = DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->select('*')
        ->where('room_status_id', '=', $room_status)
        ->get();

        return $result;

    }

    public static function DeleteRoomGroupStatus($room_status){

        DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->where('room_status_id', '=', $room_status)
        ->delete();

    }
    
    public static function DeleteRoomGroupStatusProfile($data){

        DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->whereIn('id', $data)
        ->delete();

    }

    public static function UpdateRoomGroupStatus($id, $available_status_id){

        DB::connection('rmsnew')
        ->table('tblroomgroupstatus')
        ->where('id', '=', $id)
        ->update([
            "available_status_id"=>$available_status_id,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

}
