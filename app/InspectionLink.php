<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InspectionLink extends Model
{
    //
	protected $table = 'tbl_links';
	protected $connection = 'rmsnew';

	public function getTblArea(){
		return $this->hasMany('App\EagleEye', 'id', 'id');
	}

	public function getTblComponents(){
		return $this->hasMany('App\InspectionComponent', 'id', 'id');
	}

	public function getTblStandard(){
		return $this->belongsTo('App\Standard', 'id', 'id');
	}

	public function getTblRemark(){
		return $this->belongsTo('App\Remark', 'id', 'id');
	}

	public function getFindingType(){
		return $this->belongsTo('App\FindingType', 'id', 'id');
	}
}
