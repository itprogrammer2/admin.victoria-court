<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Room_Model extends Model
{
    
    protected $table = "tblroom";
    protected $connection = "rmsnew";

    public static function GetRoomGroup(){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            'group_name',
            DB::raw("GROUP_CONCAT(room_type_id) AS roomtype")
        )
        ->groupBy('group_name')
        ->orderBy('id', 'ASC')
        ->get();

        return $result;

    }

    public static function LoadINVItems(){

        $result = DB::connection('rmsnew')
        ->table('roominventory')
        ->select(
            'id',
            'name',
            'description'
        )
        ->get();

        return $result;

    }

    public static function LoadRoomInventory($id){

        $result = DB::connection('rmsnew')
        ->table('roominventorylist')
        ->select(
            'roominventorylist.id',
            'roominventory.name',
            'roominventory.description',
            'roominventorylist.itemcode',
            'roominventorylist.brand',
            'roominventorylist.brand_description',
            'roominventorylist.serialNumber',
            'roominventorylist.trackNumber',
            'roominventorylist.differentiateValue',
            'roominventorylist.actualCost',
            'roominventorylist.quantity'
        )
        // ->leftjoin('tblroom', 'tblroom.id', '=', 'roominventorylist.room_id')
        ->leftjoin('roominventory', 'roominventory.id', '=', 'roominventorylist.room_inventory_id')
        ->where('roominventorylist.room_id', '=', $id)
        ->get();

        return $result;

    }

    public static function AddRoomInventory($data){

        DB::connection('rmsnew')
        ->table('roominventorylist')
        ->insert([
            "room_id"=>$data->id,
            "room_inventory_id"=>$data->inventoryid,
            "itemcode"=>$data->itemcode,
            "brand"=>$data->brand,
            "brand_description"=>$data->branddescription,
            "serialNumber"=>$data->serialnumber,
            "trackNumber"=>$data->tracknumber,
            "differentiateValue"=>$data->diffvalue,
            "actualCost"=>$data->actualcost,
            "quantity"=>$data->qty,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateRoomInventory($data){

        $result = DB::connection('rmsnew')
        ->table('roominventorylist')
        ->select(
            DB::raw("COUNT(*) AS 'item_count'")
        )
        ->where('room_id', '=', $data->id)
        ->where('room_inventory_id', '=', $data->inventoryid)
        ->get();

        if($result[0]->item_count==0){
            return false;
        }
        else{
            return true;
        }


    }

    public static function RemoveRoomInventory($id){

        DB::connection('rmsnew')
        ->table('roominventorylist')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function UploadRoomImage($id, $image){

        DB::connection('rmsnew')
        ->table('roomimages')
        ->insert([
            "room_id"=>$id,
            "image"=>$image,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadRoomImages($id){

        $result = DB::connection('rmsnew')
        ->table('roomimages')
        ->select(
            'id',
            'image'
        )
        ->where('room_id', '=', $id)
        ->get();

        return $result;

    }

    public static function GetRoomImage($id){

        $result = DB::connection('rmsnew')
        ->table('roomimages')
        ->select(
            'image'
        )
        ->where('id', '=', $id)
        ->get();

        return $result[0]->image;

    }

    public static function RemoveRoomImage($id){

        DB::connection('rmsnew')
        ->table('roomimages')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function LoadStatusOveride(){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'id',
            'room_status'
        )
        ->get();

        return $result;

    }


}
