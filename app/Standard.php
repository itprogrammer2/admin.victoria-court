<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standard extends Model
{
    //
    protected $table = 'tbl_standards';
    protected $connection = 'rmsnew';

    public function link(){
    	return $this->hasMany('App\InspectionLink');
    }
}
