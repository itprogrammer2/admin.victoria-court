<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InspectionComponent extends Model
{
    //
    protected $table = 'tbl_components';
    protected $connection = 'rmsnew';

    public function link(){
    	return $this->hasMany('App\InspectionLink');
    }

    public function toArea(){
    	return $this->hasMany('App\EagleEye');
    }

    public function getRemarks(){
    	return $this->belongsTo('App\Remark', 'id', 'id');
    }
}
