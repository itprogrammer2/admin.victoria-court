<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class UserAccess_Model extends Model
{
    protected $table = "";
    protected $connetion = "";

    public static function LoadUserRoles(){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            'id',
            'role'
        )
        ->get();

        return $result;

    }

    public static function LoadRoleName($id){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            'role'
        )
        ->where('id', '=', $id)
        ->get();

        return $result;

    }

    public static function LoadAllowStatus(){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'roomstatus.id',
            'roomstatus.room_status',
            DB::raw("IFNULL(GROUP_CONCAT(a.room_status), 'N/A') AS 'allow_status'")
        )
        ->leftjoin('access_levels', 'access_levels.room_status_id', '=', 'roomstatus.id')
        ->leftjoin('roomstatus as a', 'a.id', '=', 'access_levels.allow_status_id')
        ->groupBy('roomstatus.id', 'roomstatus.room_status')
        ->get();

        return $result;

    }

    public static function LoadRoomStatus(){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'id',
            'room_status'
        )
        ->get();

        return $result;

    }

    public static function ValidateNextStatus($data){

        $result = DB::connection('rmsnew')
        ->table('access_levels')
        ->select(
            DB::raw("COUNT(*) AS 'nextstatus_count'")
        )
        ->where('room_status_id', '=', $data->targetstatus)
        ->where('allow_status_id', '=', $data->nextstatus)
        ->get();

        if($result[0]->nextstatus_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveTargetStatusInfo($data){

        DB::connection('rmsnew')
        ->table('access_levels')
        ->insert([
            "room_status_id"=>$data->targetstatus,
            "allow_status_id"=>$data->nextstatus,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadAllowStatusProfile($id){

        $result = DB::connection('rmsnew')
        ->table('access_levels')
        ->select(
            'access_levels.id',
            'roomstatus.room_status'
        )
        ->leftjoin('roomstatus', 'roomstatus.id', '=', 'access_levels.allow_status_id')
        ->where('access_levels.room_status_id', '=', $id)
        ->get();

        return $result;

    }

    public static function RemoveAllowStatus($id){

        DB::connection('rmsnew')
        ->table('access_levels')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function GetRoomStatus($id){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'room_status'
        )
        ->where('id', '=', $id)
        ->get();

        return $result;

    }

}
