<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use Illuminate\Support\Collection;
use App\RoomStatus_Model as RoomStatus;
use DB;
use App\Main_Model as Main;

class RoomStatusController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null){

        if($view!=null){
            return view('extensions.roomstatus.'.$view);
        }
        else{
            return view('roomstatus');
        }
       
    }

    function RoomStatusInformation(){

        $roomstatus = RoomStatus::select('*')
                        ->get();

        $data = array();
        foreach($roomstatus as $val){

            $obj = new \stdClass;

            $obj->roomstatus = $val->room_status;
            $obj->color = '<input type="color" id="body" name="color[]"
            value="'. $val->color .'" disabled/>';
            // $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" data-toggle="modal" data-target="#uroomstatus" value="'. $val->id .'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'color'])->make(true);

    }

    function RoomStatusProfile(Request $request){

        $roomstatus =  RoomStatus::select('*')
                        ->where('id', '=', $request->id)
                        ->get();

        return json_encode([
            "room_status"=>$roomstatus[0]->room_status,
            "color"=>$roomstatus[0]->color,
        ]);

    }

    function UpdateRoomStatus(Request $request){

        RoomStatus::where('id', '=', $request->id)
                    ->update([
                        "room_status"=>$request->room_status,
                        "color"=>$request->color,
                        "updated_at"=>DB::raw("NOW()")
                    ]);

        return json_encode([
            "success"=>true,
            "message"=>"Room status information has been update."
        ]);

    }

    function GroupRoomStatusInformation(){

        $roomstatus = RoomStatus::GroupRoomStatusInformation();
        
        $data = array();
        foreach($roomstatus as $val){

            $obj = new \stdClass;

            $obj->roomstatus = $val->room_status;
            $obj->availablestatus = $val->available_room_status;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" data-toggle="modal" data-target="#ugrouproomstatus" value="'.$val->id .'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoomStatus(){

        $roomstatus = RoomStatus::select('*')
                                ->get();

        return json_encode([
            "data"=>$roomstatus
        ]);

    }

    function SaveGroupRoomStatus(Request $request){

        $user_id = Auth::user()->id;
        $validate = RoomStatus::ValidateGroupRoomStatus($request->room_status_id);

        if($validate){

            return json_encode([
                "success"=>false,
                "message"=>"This Group Room Status Already Exist."
            ]);

        }
        else{

            for($x=0;$x<count($request->available_status_id);$x++){

                RoomStatus::SaveGroupRoomStatus($request->room_status_id, $request->available_status_id[$x]["value"]);
    
            }

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add a new group status.",
                "created_at"=>DB::raw("NOW()")
            ]);
        
            return json_encode([
                "success"=>true,
                "message"=>"Group Room Status Information Has Been Save."
            ]);

        }

    }

    function LoadRoomStatusProfile(Request $request){

        $roomstatus = RoomStatus::select('room_status')
                                ->where('id', '=', $request->id)
                                ->get();

        $roomprofile = RoomStatus::LoadRoomStatusProfile($request->id);

        return json_encode([
            "roomstatus"=>$roomstatus,
            "roomprofile"=>$roomprofile
        ]);


    }

    function DeleteRoomGroupStatus(Request $request){

        $user_id = Auth::user()->id;

        //Delete Room Status
        RoomStatus::DeleteRoomGroupStatus($request->id);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Delete a group status.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Group Room Status Information Has Been Delete."
        ]);

    }

    function UpdateRoomGroupStatus(Request $request){

        $user_id = Auth::user()->id;

        //Delete Information
        if(isset($request->updatedeletearray)){ 

            RoomStatus::DeleteRoomGroupStatusProfile($request->updatedeletearray);
     
        }

        //Update Information
        for($i=0;$i<count($request->groupid);$i++){

            if($request->groupid[$i]["value"]!=0){
                
                RoomStatus::UpdateRoomGroupStatus($request->groupid[$i]["value"], $request->available_status_id[$i]["value"]);

            }
            else{

                RoomStatus::SaveGroupRoomStatus($request->id, $request->available_status_id[$i]["value"]);

            }
            
        }

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Modified a group status.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Group Room Status Information Has Been Update."
        ]);

    }

}
