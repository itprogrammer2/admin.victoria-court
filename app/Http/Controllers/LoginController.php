<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

class LoginController extends Controller
{
    
    function index(){

        return view('login');

    }

    function Login(Request $request){

       
        if(Auth::attempt(['username'=>$request->txtusername, 'passwordhash'=>$request->txtpassword])){

            Session::flash('message', "Successfully login.");
            return redirect('/main'); 

        }
        else{

            Session::flash('message', "Invalid username or password.");
            return redirect()->back();

        }


    }

    function Logout(){

        Auth::logout();
        return;

    }

}
