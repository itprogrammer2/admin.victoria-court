<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Reservation_Model as Reservation;
use DataTables;
use Illuminate\Support\Collection;
use App\Main_Model as Main;

class ReservationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('reservation');

    }

    function LoadReservationInformation(){

        $reservation = Reservation::select(
            'id',
            'name',
            'description'
        )
        ->get();

        $data = array();
        foreach($reservation as $val){

            $obj = new \stdClass;

            $obj->name = $val->name;
            $obj->description = $val->description;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'.$val->id .'" data-toggle="modal" data-target="#updatereservation"><i class="fa fa-pencil"></i></button> <button id="btnremove" name="btnremove" class="btn btn-danger btn-flat" title="Remove" value="'.$val->id .'"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function NewReservation(Request $request){

        $user_id = Auth::user()->id;
        $validation = Reservation::select(
            DB::raw("COUNT(*) AS 'reservation_count'")
        )
        ->where('name', '=', $request->name)
        ->get();

        if($validation[0]->reservation_count==0){

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add reservation information.",
                "created_at"=>DB::raw("NOW()")
            ]);

            Reservation::insert([
                "name"=>$request->name,
                "description"=>$request->description,
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Reservation Information Has Been Added."
            ]);

        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"Reservation Name Already Exist."
            ]);

        }

    }

    function RemoveReservation(Request $request){

        $user_id = Auth::user()->id;

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Remove reservation information.",
            "created_at"=>DB::raw("NOW()")
        ]);

        Reservation::where('id', '=', $request->id)
        ->delete();

        return json_encode([
            "success"=>true,
            "message"=>"Reservation Information Has Been Remove."
        ]);


    }

    function UpdateReservation(Request $request){

        $user_id = Auth::user()->id;

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Update reservation information.",
            "created_at"=>DB::raw("NOW()")
        ]);

        Reservation::where('id', '=', $request->id)
        ->update([
           "name"=>$request->name,
           "description"=>$request->description,
           "updated_at"=>DB::raw("NOW()") 
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Reservation Information Has Been Update."
        ]);

    }

    function GetReservationProfile(Request $request){

        $reservationprofile = Reservation::select(
            'name',
            'description'
        )
        ->where('id', '=', $request->id)
        ->get();

        return json_encode([
            "name"=>$reservationprofile[0]->name,
            "description"=>$reservationprofile[0]->description
        ]);

    }

}
