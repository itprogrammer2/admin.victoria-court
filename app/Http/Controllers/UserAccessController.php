<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAccess_Model as UserAccess;
use Auth;
use DB;
use DataTables;
use Illuminate\Support\Collection;
use App\Main_Model as Main;

class UserAccessController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null, $id=null){

        if($view!=null && $id!=null){

            return view('extensions.useraccess.'.$view)->with('id', $id);

        }
        else{
            return view('useraccess');
        }
       
    }

    function LoadUserRoles(){

        $userrole = UserAccess::LoadUserRoles();

        $data = array();
        foreach($userrole as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->role = $val->role;
            $obj->panel = '<a href="/useraccess/useraccessprofile/'.$val->id.'"><button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit"><i class="fa fa-pencil"></i></button></a>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoleName(Request $request){

        $role = UserAccess::LoadRoleName($request->id);

        return json_encode([
            "role"=>$role[0]->role
        ]);

    }

    function LoadAllowStatus(Request $request){

        $allowroles = UserAccess::LoadAllowStatus();

        $data = array();
        foreach($allowroles as $val){

            $obj = new \stdClass;

            $obj->status = $val->room_status;
            $obj->allowstatus = $val->allow_status;
            $obj->panel = '<button id="btnremoveroomstatus" name="btnremoveroomstatus" class="btn btn-flat btn-primary" data-toggle="modal" data-target="#useraccessroomstatusinfo" value="'.$val->id.'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoomStatus(){

        $roomstatus = UserAccess::LoadRoomStatus();

        return json_encode([
            "data"=>$roomstatus
        ]);

    }

    function SaveTargetStatusInfo(Request $request){

        $validation = UserAccess::ValidateNextStatus($request);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Next status already exist please select another status."
            ]);

        }
        else{

            UserAccess::SaveTargetStatusInfo($request);

            return json_encode([
                "success"=>true,
                "message"=>"User Room Status Has Been Save."
            ]);

        }

    }

    function LoadAllowStatusProfile(Request $request){

        $status = UserAccess::GetRoomStatus($request->id);
        $allowstatus = UserAccess::LoadAllowStatusProfile($request->id);

        return json_encode([
            "status"=>$status[0]->room_status,
            "data"=>$allowstatus
        ]);

    }

    function RemoveAllowStatus(Request $request){

        UserAccess::RemoveAllowStatus($request->id);

        return json_encode([
            "success"=>true,
            "message"=>"Allow Status Has Been Remove."
        ]);

    }

}
