<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EagleEye as EagleEye;
use App\InspectionComponent as Components;
use App\Standard as Standard;
use App\Remark;
use App\FindingType;
use App\InspectionLink as Link;
use DB;
use Illuminate\Support\Collection;
use DataTables;
use Response;

class EagleEyeController extends Controller
{
    //
    public function index(){
    	return view('inspection.eagleeye');
    }

    public function components(){
    	return view('inspection.components');
    }

    public function standard(){
    	return view('inspection.standard');
    }

    public function remarks(){
    	return view('inspection.remarks');
    }

    public function findings(){
    	return view('inspection.finding');
    }

    public function getRoomInspection(){
    	$query = EagleEye::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$data[] = $obj;
    	}

    	$info = new Collection($data);
    	return DataTables::of($info)->rawColumns(['name'])->make(true);
    }

    public function saveRoomForInspection(Request $r){

    	$this->validate($r, [
    		'room' => 'required|string',
    	]);

    	EagleEye::insert([
    		'name' => $r->room,
    		'created_at' => DB::raw("NOW()")
    	]);

    	return json_encode([
    		"success" => true,
    		"message" => 'Inspection Location Added Succesfully'
    	]);

    }

    public function getComponentInspection(){
    	$query = Components::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$data[] = $obj;
    	}
    	$info = new Collection($data);
    	return DataTables::of($info)->rawColumns(['name'])->make(true);

    }

    public function saveComponentForInspection(Request $r){

    	$this->validate($r, [
    		'component' => 'required|string',
    	]);

    	Components::insert([
    		'name' => $r->component,
    		'created_at' => DB::raw("NOW()"),
    	]);

    	return json_encode([
    		"success" => true,
    		"message" => 'Inspection Components Added Successfully'
    	]);

    }

    public function getStandardInspection(){
    	$query = Standard::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$obj->description = $out->description;
    		$data[] = $obj;
    	}
    	$info = new Collection($data);
    	return DataTables::of($info)->make(true);
    }

    public function saveStandardForInspection(Request $r){

    	Standard::insert([
    		'name' => $r->standardName,
    		'description' => $r->standardDescription,
    		'created_at' => DB::raw("NOW()"),
    	]);

    	return json_encode([
    		'success' => true,
    		'message' => 'Inspection Standard Added Successfully'
    	]);

    }

    public function saveRemarksForInspection(Request $r){

    	Remark::insert([
    		'name' => $r->inspectionremarks,
    		'created_at' => DB::raw("NOW()"),
    	]);

    	return json_encode([
    		'success' => true,
    		'message' => 'Inspection Remarks Added Successfully'
    	]);

    }

    public function getRemarksInspection(){
    	$query = Remark::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$data[] = $obj;
    	}

    	$info = new Collection($data);
    	return DataTables::of($info)->make(true);
    }

    public function saveFindingsForInspection(Request $r){
        // return $r->inspectionFinding;

    	FindingType::insert([
            'name' => $r->inspectionFinding,
            'created_at' => date('Y-m-d H:i:s')
        ]);

    	return json_encode([
    		'success' => true,
    		'message' => 'Inspection Finding Type Added Successfully', 
    	]);

    }

    public function getFindingsInspection(){
        $query = FindingType::get();

        $data = array();
        foreach($query as $out){
            $obj = new \stdClass;
            $obj->name = $out->name;
            $data[] = $obj;
        }

        $info = new Collection($data);
        return DataTables::of($info)->make(true);
    }

    public function forTblLink(){

        // use App\EagleEye as EagleEye;
        // use App\InspectionComponent as Components;
        // use App\Standard as Standard;
        // use App\Remark;
        // use App\FindingType;
        $area = EagleEye::select('id', 'name')->get();
        $components = Components::select('id', 'name')->get();
        $standard = Standard::select('id', 'name', 'description')->get();
        $remarks = Remark::select('id', 'name')->get();
        $findingType = FindingType::select('id', 'name')->get();

        $query = EagleEye::select('tbl_areas.id as area_id', 'tbl_areas.name as area_name',
                                    'tbl_components.id as component_id', 'tbl_components.name as component_name',
                                    'tbl_standards.id as standard_id', 'tbl_standards.name as standard_name', 'tbl_standards.description as standard_description',
                                    'tbl_remarks.id as remarks_id', 'tbl_remarks.name as remarks_name',
                                    'tbl_findings_type.id as finding_type_id', 'tbl_findings_type.name as finding_type_name')
                ->join('tbl_components', 'tbl_areas.id', '=', 'tbl_components.id')
                ->join('tbl_standards', 'tbl_areas.id', '=', 'tbl_standards.id')
                ->join('tbl_remarks', 'tbl_areas.id', '=', 'tbl_remarks.id')
                ->join('tbl_findings_type', 'tbl_areas.id', '=', 'tbl_findings_type.id')
                ->get();

        // $query = InspectionLink::with('getTblArea', 'getTblComponents', 'getTblStandard', 'getTblRemark', 'getFindingType')
        //                 ->where('tbl_areas.id', '=', $r->id)
        //                 ->get();

        return json_encode([
            'wholeResponse' => $query,
            'tblArea' => $area,
            'components' => $components,
            'standard' => $standard,
            'remarks' => $remarks,
            'findingType' => $findingType,
        ]);
        
    }

    public function getAreas(Request $r){

        $query = Link::with('getTblArea')
                    ->where('id', '=', $r->areaId)
                    ->get();
        return json_encode([
            'getAllArea' => $query
        ]);
    }

    // public function getComponent($areaId, $remarksId = null){
    public function getComponents(Request $r){

        $query = Link::with('getTblArea', 'getTblComponents', 'getTblRemark')
                        ->where('area_id', '=', $r->areaId)
                        ->where('component_id', '=', $r->componentId)
                        ->where('remarks_id', '=', $r->remarks)
                        ->get();
        if($query){
            return json_encode([
                'areaWithComponent' => $query
            ]);
        }else{
            return json_encode([
                'message' => "There's an error please contact backend devevloper error at parsing id's"
            ]);
        }
        
    }
}
