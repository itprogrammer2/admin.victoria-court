<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room_Model as Room;
use Auth;
use DataTables;
use DB;
use App\Main_Model as Main;
use Illuminate\Support\Collection;
use Storage;

class RoomController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null, $id=null){

        if($view!=null && $id!=null){
            return view('extensions.room.roomprofile')->with('id', $id);
        }
        else{
            return view('rooms');
        }

    }

    function LoadRoomInformation(){

        $roomgroup = Room::GetRoomGroup();
 
        $room = Room::select(
            'tblroom.id',
            'tblroom.room_no',
            'tblroom.room_type_id'
        )
        ->orderBy('tblroom.room_area_id', 'asc')
        ->orderBy('tblroom.room_type_id', 'asc')
        ->get();

        return view('extensions.room.roomstable')->with('roomgroup', $roomgroup)->with('room', $room);

    }

    function LoadRoomProfile(Request $request){

        $roomprofile = Room::select(
            'tblroom.room_no',
            'tblroom.room_name',
            'tblroom.room_description',
            'tblroomtype.room_type'
        )
        ->leftjoin('tblroomtype', 'tblroomtype.id', '=', 'tblroom.room_type_id')
        ->where('tblroom.id', '=', $request->id)
        ->get();

        return json_encode([
            "data"=>$roomprofile
        ]);

    }

    function UpdateRoomDetails(Request $request){

        $user_id = Auth::user()->id;

        //Update
        Room::where('id', '=', $request->id)
        ->update([
            "room_name"=>$request->roomname,
            "room_description"=>$request->roomdescription,
            "updated_at"=>DB::raw("NOW()")
        ]);
        
        //Insert System Activity
        Main::insert([
           "user_id"=>$user_id,
           "activity"=>"Room ". $request->roomno ." Details Has Been Update.",
           "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Room Details Has Been Update."
        ]);

    }

    function LoadINVItems(){

        $roominventory = Room::LoadINVItems();

        return json_encode([
            "data"=>$roominventory
        ]);

    }

    function LoadRoomInventory(Request $request){

        $roominventory = Room::LoadRoomInventory($request->id);

        $data = array();
        foreach($roominventory as $val){

            $obj = new \stdClass;

            $obj->itemname = $val->name;
            $obj->itemdescription = $val->description;
            $obj->itemcode = $val->itemcode;
            $obj->brand = $val->brand;
            $obj->branddescription = $val->brand_description;
            $obj->serialNumber = $val->serialNumber;
            $obj->trackNumber = $val->trackNumber;
            $obj->differentiateValue = $val->differentiateValue;
            $obj->actualCost = $val->actualCost;
            $obj->qty = $val->quantity;
            $obj->panel = '<button id="btninvremove" name="btninvremove" class="btn btn-danger btn-flat" title="Remove" value="'.$val->id .'" style="font-size: 10px;"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function AddRoomInventory(Request $request){

        $user_id = Auth::user()->id;

        $roomno = Room::select(
            'room_no'
        )
        ->where('id', '=', $request->id)
        ->get();

        $validation = Room::ValidateRoomInventory($request);


        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Inventory Item Already Exist."
            ]);

        }
        else{

            Room::AddRoomInventory($request);

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add a new inventory item list ".$roomno[0]->room_no.".",
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Inventory Item Has Been Added."
            ]);


        }

    }

    function RemoveRoomInventory(Request $request){

        $user_id = Auth::user()->id;

        $roomno = Room::select(
            'room_no'
        )
        ->where('id', '=', DB::raw("(SELECT room_id FROM roominventorylist WHERE id='".$request->id."')"))
        ->get();

        Room::RemoveRoomInventory($request->id);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Remove a new inventory item list ".$roomno[0]->room_no.".",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Inventory Item Has Been Remove."
        ]);

    }

    function UploadRoomImage(Request $request){

        if($request->hasFile('uploadimage')){
            
            $user_id = Auth::user()->id;

            $roomno = Room::select(
                'room_no'
            )
            ->where('id', '=', $request->txtuploadid)
            ->get();

            //Storage::putFile('public/images', $request->file('image'));
            $request->uploadimage->storeAs('public', $request->uploadimage->getClientOriginalName());
            Room::UploadRoomImage($request->txtuploadid, $request->uploadimage->getClientOriginalName());

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Upload a new image in ".$roomno[0]->room_no.".",
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Successfully Upload Image."
            ]);

        }

    }

    function LoadRoomImages(Request $request){

        $roomimage = Room::LoadRoomImages($request->id);

        return view('extensions.room.roomimage')->with('roomimage', $roomimage);

    }

    function RemoveRoomImage(Request $request){

        $user_id = Auth::user()->id;
        $filename = Room::GetRoomImage($request->imageid);

        $roomno = Room::select(
            'room_no'
        )
        ->where('id', '=', DB::raw("(SELECT room_id FROM roomimages WHERE id='".$request->imageid."')"))
        ->get();

        Room::RemoveRoomImage($request->imageid);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Remove a image in ".$roomno[0]->room_no.".",
            "created_at"=>DB::raw("NOW()")
        ]);

        unlink(storage_path('app/public/'.$filename));

        return json_encode([
            "success"=>true,
            "message"=>"Successfully Remove Image."
        ]);

    }

    function LoadStatusOveride(){

        $statusoveride = Room::LoadStatusOveride();

        return json_encode([
            "data"=>$statusoveride
        ]);

    }

    function OverideRoomStatus(Request $request){

        $user_id = Auth::user()->id;
        $prevstatus = Room::select(
            'room_no',
            'room_status_id'
        )
        ->where('id', '=', $request->id)
        ->get();

        Room::where('id', '=', $request->id)
        ->update([
            "room_status_id"=>$request->overidestatusid,
            "from_room_status_id"=>$prevstatus[0]->room_status_id,
            "updated_at"=>DB::raw("NOW()")
        ]);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Status has been overide in ".$prevstatus[0]->room_no.".",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Room Status Has Been Overide."
        ]);

    }

}
