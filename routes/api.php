<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Login API
Route::post('login/loginuser', 'LoginController@Login');
Route::post('login/logoutuser', 'LoginController@Logout');

//Room Status API
Route::get('roomstatus/roomstatusinformation', 'RoomStatusController@RoomStatusInformation');
Route::get('roomstatus/roomstatusprofile', 'RoomStatusController@RoomStatusProfile');
Route::post('roomstatus/updateroomstatus', 'RoomStatusController@UpdateRoomStatus');
Route::get('roomstatus/grouproomstatusinformation', 'RoomStatusController@GroupRoomStatusInformation');
Route::get('roomstatus/loadroomstatus', 'RoomStatusController@LoadRoomStatus');
Route::post('roomstatus/savegrouproomstatus', 'RoomStatusController@SaveGroupRoomStatus');
Route::get('roomstatus/loadroomstatusprofile', 'RoomStatusController@LoadRoomStatusProfile');
Route::post('roomstatus/deleteroomgroupstatus', 'RoomStatusController@DeleteRoomGroupStatus');
Route::post('roomstatus/updateroomgroupstatus', 'RoomStatusController@UpdateRoomGroupStatus');

//Room API
Route::get('room/loadroomsinformation', 'RoomController@LoadRoomInformation');
Route::get('room/loadroomprofile', 'RoomController@LoadRoomProfile');
Route::post('room/updateroomdetails', 'RoomController@UpdateRoomDetails');
Route::get('room/loadinvitems', 'RoomController@LoadINVItems');
Route::get('room/loadroominventory', 'RoomController@LoadRoomInventory');
Route::post('room/addroominventory', 'RoomController@AddRoomInventory');
Route::post('room/removeroominventory', 'RoomController@RemoveRoomInventory');
Route::post('room/uploadroomimage', 'RoomController@UploadRoomImage');
Route::get('room/loadroomimages', 'RoomController@LoadRoomImages');
Route::post('room/removeroomimage', 'RoomController@RemoveRoomImage');
Route::get('room/loadstatusoveride', 'RoomController@LoadStatusOveride');
Route::post('room/overideroomstatus', 'RoomController@OverideRoomStatus');


// Inspection API
Route::get('inspection/room/getInfo', 'EagleEyeController@getRoomInspection');
Route::post('inspection/room', 'EagleEyeController@saveRoomForInspection');
Route::get('inspection/component/getInfo', 'EagleEyeController@getComponentInspection');
Route::post('inspection/component', 'EagleEyeController@saveComponentForInspection');
Route::get('inspection/standard/getInfo', 'EagleEyeController@getStandardInspection');
Route::post('inspection/standard', 'EagleEyeController@saveStandardForInspection');
Route::get('inspection/remarks/getInfo', 'EagleEyeController@getRemarksInspection');
Route::post('inspection/remarks', 'EagleEyeController@saveRemarksForInspection');
Route::get('inspection/finding/getInfo', 'EagleEyeController@getFindingsInspection');
Route::post('inspection/finding', 'EagleEyeController@saveFindingsForInspection');

// inspection for mobile
Route::get('getDetails/', 'EagleEyeController@forTblLink');
// area
Route::get('getArea/area/{areaId?}', 'EagleEyeController@getAreas');
// components
Route::get('getComponent/area/{areaId}/component/{componentId}/remarks/{remarks?}', 'EagleEyeController@getComponents');

//Main API
Route::get('systemactivities/loadsystemactivities', 'MainController@LoadSystemActivities');

//Inventory API
Route::get('inventory/loadinventoryinformation', 'InventoryController@LoadInventoryInformation');
Route::post('inventory/newinventory', 'InventoryController@NewInventory');
Route::post('inventory/removeinventory', 'InventoryController@RemoveInventory');
Route::get('inventory/profileinventory', 'InventoryController@ProfileInventory');
Route::post('inventory/updateinventory', 'InventoryController@UpdateInventory');

//Vehicle API
Route::get('vehicle/loadvehicleinformation', 'VehiclesController@LoadVehicleInformation');
Route::post('vehicle/newvehicle', 'VehiclesController@NewVehicle');
Route::post('vehicle/removevehicle', 'VehiclesController@RemoveVehicle');
Route::get('vehicle/profilevehicle', 'VehiclesController@ProfileVehicle');
Route::post('vehicle/updatevehicle', 'VehiclesController@UpdateVehicle');
Route::get('vehicle/validatevehiclename', 'VehiclesController@ValidateVehicleName');

//Reservation API
Route::get('reservation/loadreservationinformation', 'ReservationController@LoadReservationInformation');
Route::post('reservation/newreservation', 'ReservationController@NewReservation');
Route::post('reservation/removereservation', 'ReservationController@RemoveReservation');
Route::post('reservation/updatereservation', 'ReservationController@UpdateReservation');
Route::get('reservation/getreservationprofile', 'ReservationController@GetReservationProfile');

//User Access And User Access Profile API
Route::get('useraccess/loaduserroles', 'UserAccessController@LoadUserRoles');
Route::get('useraccessprofile/loadrolename', 'UserAccessController@LoadRoleName');
Route::get('useraccessprofile/loadallowstatus', 'UserAccessController@LoadAllowStatus');
Route::get('useraccessprofile/loadroomstatus', 'UserAccessController@LoadRoomStatus');
Route::post('useraccessprofile/savetargetstatusinfo', 'UserAccessController@SaveTargetStatusInfo');
Route::get('useraccessprofile/loadallowstatusprofile', 'UserAccessController@LoadAllowStatusProfile');
Route::post('useraccessprofile/removeallowstatus', 'UserAccessController@RemoveAllowStatus');


