<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index');
Route::get('/login', 'LoginController@index')->name('login');
Route::get('/main', 'MainController@index');
Route::get('/roomstatus/{view?}', 'RoomStatusController@index');
Route::get('/rooms/{view?}/{id?}', 'RoomController@index');
Route::get('/eagleye', 'EagleEyeController@index');
Route::get('/components', 'EagleEyeController@components');
Route::get('/standard', 'EagleEyeController@standard');
Route::get('/remarks', 'EagleEyeController@remarks');
Route::get('/findings', 'EagleEyeController@findings');
Route::get('/inventory', 'InventoryController@index');
Route::get('/vehicle', 'VehiclesController@index');
Route::get('/reservation', 'ReservationController@index');
Route::get('/useraccess/{view?}/{id?}', 'UserAccessController@index');